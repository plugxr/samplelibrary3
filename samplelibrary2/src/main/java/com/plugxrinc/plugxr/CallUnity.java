package com.plugxrinc.plugxr;

import android.content.Context;
import android.content.Intent;

public class CallUnity {

    public static void call(Context context){
        Intent intent = new Intent(context,UnityPlayerActivity.class);
        context.startActivity(intent);
    }
}
